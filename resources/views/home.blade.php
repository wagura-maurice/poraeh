@extends('layouts.app')

@section('content')

<style type="text/css">
    /* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <!-- Tab links -->
                <div class="tab col-md-offset-2" style="margin-top: 2em;">
                  <button class="tablinks btn btn-info" onclick="openTab(event, 'my_account')">My Account</button>
                  <button class="tablinks btn btn-info" onclick="openTab(event, 'send_money')">Send Money</button>
                  <button class="tablinks btn btn-info" onclick="openTab(event, 'withdraw_money')">Withdraw Money</button>
                  <button class="tablinks btn btn-info" onclick="openTab(event, 'buy_airtime')">Buy Airtime</button>
                </div>

                <hr>

                <div class="panel-body" style="text-align: center;">
                    <!-- Tab content -->
                    <div id="my_account" class="tabcontent">
                      <h3>My Account</h3>
                      <p>Current Account balance : <strong>{{ $currency }} {{ number_format($amount, 2) }}</strong></p>
                    </div>

                    <div id="send_money" class="tabcontent">
                      <h3>Send Money</h3>
                      <p>Minimum Possible : <strong>{{ $currency }} {{ number_format(5, 2) }}</strong></p>
                      
                      <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Amount</label>

                            <div class="col-md-6">
                                <input id="amount" type="number" class="form-control" name="amount" value="{{ old('amount') }}" min="5" max="{{ $amount }}" required autofocus>

                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}">
                            <label for="user" class="col-md-4 control-label">Send to User</label>

                            <div class="col-md-6">
                                <select name="user" id="user" class="form-control edited" required="true">
                                    <option selected disabled>-- Select User--</option>
                                    @foreach ($Users as $User)
                                        <option value="{{ $User->id }}">{{ strtoupper($User->name) . " : : " . $User->phone }}</small></option>    
                                    @endforeach
                                </select>
                                @if ($errors->has('user'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Send Money</button>
                            </div>
                        </div>
                    </form>

                    </div>

                    <div id="withdraw_money" class="tabcontent">
                      <h3>Withdraw Money</h3>
                      <p>Comming Soon</p>
                    </div>

                    <div id="buy_airtime" class="tabcontent">
                      <h3>Buy Airtime</h3>
                      <p>Comming Soon</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

@endsection
