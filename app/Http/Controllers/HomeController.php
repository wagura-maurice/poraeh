<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coffer;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data = Coffer::where('id', auth()->user()->id)->first();

        return view('home', ["currency" => $data->currency, "amount" => $data->amount, "Users" => User::where('id', '!=', auth()->user()->id)->get()]);
    }

    public function store(Request $request) {

        if (Coffer::where('id', $request['user'])->update(['amount' => Coffer::where('id', auth()->user()->id)->first()->amount + $request['amount']])) {
            $this->send_toReceiver(User::where('id', $request['user'])->first()->phone, User::where('id', $request['user'])->first()->name, $request['amount'], User::where('id', auth()->user()->id)->first()->name, $request['amount'] + Coffer::where('id', $request['user'])->first()->amount);
        }

       if (Coffer::where('id', auth()->user()->id)->update(['amount' => Coffer::where('id', auth()->user()->id)->first()->amount - $request['amount']])) {
            $this->send_toSender(User::where('id', auth()->user()->id)->first()->phone, User::where('id', auth()->user()->id)->first()->name, $request['amount'], User::where('id', $request['user'])->first()->name, User::where('id', auth()->user()->id)->first()->amount - Coffer::where('id', $request['user'])->first()->amount);
        }

    }

    private function send_toReceiver($phone, $name, $amount, $from, $balance) {
        $client = new Client();
        $response = $client->get("http://{$SMS_host}/bulksms/bulksms?username={$SMS_username}&password={$SMS_password}&type=0&dlr=1&destination={$SMS_phone}&source={$SMS_senderID}&message=HI, {$name}. You have received {$amount} from {$from}, Your New balance is {$balance}");
        // $response = json_decode($response->getBody(), true);
    }

    private function send_toSender($phone, $name, $amount, $to, $balance) {
        $client = new Client();
        $response = $client->get("http://{$SMS_host}/bulksms/bulksms?username={$SMS_username}&password={$SMS_password}&type=0&dlr=1&destination={$SMS_phone}&source={$SMS_senderID}&message=HI, {$name}. You have sent {$amount} to {$from}, Your New balance is {$balance}");
        // $response = json_decode($response->getBody(), true);
    }

}
