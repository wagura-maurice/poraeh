<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Coffer;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {

        $data['phone'] = "254".substr($data['phone'], -9);

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            // 'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {

        $data['password'] = str_random(8);

        $insert_User = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => "254".substr($data['phone'], -9),
            'password' => bcrypt($data['password'])
        ]);

        $insert_Coffer = Coffer::create([
            'user_id' => $insert_User->id,
            'amount' => '100'
        ]);

        $this->sendOTP($data['phone'], $data['name'], $data['password']);

        return $insert_User;
    }

    private function sendOTP($phone, $name, $password) {
        $client = new Client();
        $response = $client->get("http://121.241.242.114/bulksms/bulksms?username=oz11-movetechke&password=mwiti&type=0&dlr=1&destination={$phone}&source=Movetech&message=HI, {$name}. Use {$password} as your password to login");
        // $response = json_decode($response->getBody(), true);
    }
}
