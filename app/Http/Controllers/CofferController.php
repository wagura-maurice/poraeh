<?php

namespace App\Http\Controllers;

use App\Coffer;
use Illuminate\Http\Request;

class CofferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coffer  $coffer
     * @return \Illuminate\Http\Response
     */
    public function show(Coffer $coffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coffer  $coffer
     * @return \Illuminate\Http\Response
     */
    public function edit(Coffer $coffer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coffer  $coffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coffer $coffer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coffer  $coffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coffer $coffer)
    {
        //
    }
}
