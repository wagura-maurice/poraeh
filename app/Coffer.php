<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coffer extends Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coffers';

    protected $fillable = [
        'user_id', 'amount', 'currency_id',
    ];
}
